# Snake the Game

The classic game of Snake built for Linux!

<p align="center">
  <img src="https://gitlab.com/Alec_Tan/snake-the-game/-/raw/master/media/Snake_Game.gif" alt="animated" />
</p>

## Requirements
1. GCC and Make (See installation instructions for your Linux Distro. )
2. SDL (See: https://wiki.libsdl.org/Installation)

## Installation
To install:
1. Install pre-requisite software (See **Requirements**)
2. Clone/download Snake_the_Game
3. Run the `make` command on the same directory as the repo
4. Run `run_snake`

## Controls
- Directional Keys : Change Direction of Snake
- q                : Quit
- r                : Restart

## Future Changes & Fixes
- Remove the white square that appears on the top left whenever the game starts/restarts
- Add a start menu
- Add a scoring system
- Add music and Sound FX

